<?php

namespace Drupal\trash_event_dispatcher\Event;

use Drupal\core_event_dispatcher\Event\Entity\AbstractEntityEvent;
use Drupal\trash_event_dispatcher\TrashHookEvents;

/**
 * Defines an event triggered before an entity is soft-deleted.
 *
 * @HookEvent(
 *   id = "entity_pre_trash_delete",
 *   hook = "entity_pre_trash_delete"
 * )
 */
class EntityPreTrashDeleteEvent extends AbstractEntityEvent {

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return TrashHookEvents::ENTITY_PRE_TRASH_DELETE;
  }

}
