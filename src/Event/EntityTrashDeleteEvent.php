<?php

namespace Drupal\trash_event_dispatcher\Event;

use Drupal\core_event_dispatcher\Event\Entity\AbstractEntityEvent;
use Drupal\trash_event_dispatcher\TrashHookEvents;

/**
 * Defines an event triggered when an entity is soft-deleted.
 *
 * @HookEvent(
 *   id = "entity_trash_delete",
 *   hook = "entity_trash_delete"
 * )
 */
class EntityTrashDeleteEvent extends AbstractEntityEvent {

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return TrashHookEvents::ENTITY_TRASH_DELETE;
  }

}
