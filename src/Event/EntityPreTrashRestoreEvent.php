<?php

namespace Drupal\trash_event_dispatcher\Event;

use Drupal\core_event_dispatcher\Event\Entity\AbstractEntityEvent;
use Drupal\trash_event_dispatcher\TrashHookEvents;

/**
 * Defines an event triggered before an entity is restored from the trash.
 *
 * @HookEvent(
 *   id = "entity_pre_trash_restore",
 *   hook = "entity_pre_trash_restore"
 * )
 */
class EntityPreTrashRestoreEvent extends AbstractEntityEvent {

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return TrashHookEvents::ENTITY_PRE_TRASH_RESTORE;
  }

}
