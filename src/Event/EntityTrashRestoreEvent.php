<?php

namespace Drupal\trash_event_dispatcher\Event;

use Drupal\core_event_dispatcher\Event\Entity\AbstractEntityEvent;
use Drupal\trash_event_dispatcher\TrashHookEvents;

/**
 * Defines an event triggered when an entity is restored from the trash.
 *
 * @HookEvent(
 *   id = "entity_trash_restore",
 *   hook = "entity_trash_restore"
 * )
 */
class EntityTrashRestoreEvent extends AbstractEntityEvent {

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return TrashHookEvents::ENTITY_TRASH_RESTORE;
  }

}
