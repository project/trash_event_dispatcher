<?php

namespace Drupal\trash_event_dispatcher;

use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Defines events for Trash hooks.
 */
final class TrashHookEvents {

  /**
   * Respond before entity soft deletion.
   *
   * @Event
   *
   * @see \Drupal\trash_event_dispatcher\Event\EntityPreTrashDeleteEvent
   * @see trash_event_dispatcher_entity_pre_trash_delete()
   * @see hook_entity_pre_trash_delete()
   *
   * @var string
   */
  public const ENTITY_PRE_TRASH_DELETE = HookEventDispatcherInterface::PREFIX . 'entity.pre_trash_delete';

  /**
   * Respond to entity soft deletion.
   *
   * @Event
   *
   * @see \Drupal\trash_event_dispatcher\Event\EntityTrashDeleteEvent
   * @see trash_event_dispatcher_entity_trash_delete()
   * @see hook_entity_trash_delete()
   *
   * @var string
   */
  public const ENTITY_TRASH_DELETE = HookEventDispatcherInterface::PREFIX . 'entity.trash_delete';

  /**
   * Respond before restoring a soft deleted entity.
   *
   * @Event
   *
   * @see \Drupal\trash_event_dispatcher\Event\EntityPreTrashDeleteEvent
   * @see trash_event_dispatcher_entity_pre_trash_restore()
   * @see hook_entity_pre_trash_restore()
   *
   * @var string
   */
  public const ENTITY_PRE_TRASH_RESTORE = HookEventDispatcherInterface::PREFIX . 'entity.pre_trash_restore';

  /**
   * Respond to restoring a soft deleted entity.
   *
   * @Event
   *
   * @see \Drupal\trash_event_dispatcher\Event\EntityTrashDeleteEvent
   * @see trash_event_dispatcher_entity_trash_restore()
   * @see hook_entity_trash_restore()
   *
   * @var string
   */
  public const ENTITY_TRASH_RESTORE = HookEventDispatcherInterface::PREFIX . 'entity.trash_restore';

}
